﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonADONet.Controllers
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
    }
    public class PersonController : Controller
    {
        //
        // GET: /Person/

        public ActionResult Index()
        {
            string query = "SELECT * from People";
            //Windows Authentication
            string connectionString = @"data source = .\SQLExpress; initial catalog= DemoADONet; integrated security = true";
            //SQL Server Authentication
            //string connectionString = @"data source = .\SQLExpress; initial catalog= DemoADONet; user id = SA; password = P@$$w0rd";
          //  string name="";
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader dr = command.ExecuteReader();

            List<Person> pList = new List<Person>();

            while (dr.Read())
            {
                Person p = new Person();
                p.Id = Convert.ToInt32(dr["Id"]);
                p.Name = dr["Name"].ToString();
                p.Email = dr["Email"].ToString();
                p.Age = Convert.ToInt32(dr["Age"]);

                pList.Add(p);

             }
            
            ViewBag.Table = pList;
            connection.Close();
            return View();
        }
        public ActionResult Insert()
        {
            string query1 = "INSERT into People (Name, Email, Age) Values ('Tanvir', 'tanvir@atp2.com', 21)";
            string query2 = "SELECT * from People";
            string connectionString = ConfigurationManager.ConnectionStrings["DemoADONet"].ConnectionString ;
            //  string name="";
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command1 = new SqlCommand(query1, connection);
            connection.Open();

            int x=command1.ExecuteNonQuery();
            SqlCommand command2 = new SqlCommand(query2, connection);
            SqlDataReader dr = command2.ExecuteReader();

            List<Person> pList = new List<Person>();

            while (dr.Read())
            {
                Person p = new Person();
                p.Id = Convert.ToInt32(dr["Id"]);
                p.Name = dr["Name"].ToString();
                p.Email = dr["Email"].ToString();
                p.Age = Convert.ToInt32(dr["Age"]);

                pList.Add(p);

            }

            ViewBag.Table = pList;
            connection.Close();
            return RedirectToAction("Index");
        }
    }
}
