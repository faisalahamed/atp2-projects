﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonADONet.Controllers
{
    public class AnotherPersonController : Controller
    {
        //
        // GET: /AnotherPerson/

        public ActionResult Index()
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["DemoADONet"].ConnectionString;
            string query = "SELECT * from People";
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(query, connection);

            DataSet ds = new DataSet();

            adapter.Fill(ds, "People");

            ds.Tables["People"].PrimaryKey = new DataColumn[] { ds.Tables["People"].Columns["Id"] };
            ds.Tables["People"].Columns["Id"].AutoIncrement = true;
            ds.Tables["People"].Columns["Id"].AutoIncrementSeed = 1;
            ds.Tables["People"].Columns["Id"].AutoIncrementStep = 1;
           
            DataView dv = new DataView(ds.Tables["People"]);
            ViewBag.Table = dv;

         //   DataRow dr = ds.Tables["People"].Rows.Find(3);
        //    return dr["Name"].ToString();
            return View();
        }

        public ActionResult Insert()
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["DemoADONet"].ConnectionString;
            string query = "SELECT * from People";
            
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
            SqlCommandBuilder builder = new SqlCommandBuilder(adapter);
            
            DataSet ds = new DataSet();

            adapter.Fill(ds, "People");

            ds.Tables["People"].PrimaryKey = new DataColumn[] { ds.Tables["People"].Columns["Id"] };
            ds.Tables["People"].Columns["Id"].AutoIncrement = true;
            ds.Tables["People"].Columns["Id"].AutoIncrementSeed = ds.Tables["People"].Rows.Count +1;
            ds.Tables["People"].Columns["Id"].AutoIncrementStep = 1;

            DataRow dr = ds.Tables["People"].NewRow();
           // dr["Id"] = -3;
            dr["Name"] = "Zahid";
            dr["Email"] = "zahid@atp2.com";
            dr["Age"] = 20;

            ds.Tables["People"].Rows.Add(dr);
            adapter.Update(ds.Tables["People"]);
            adapter.Fill(ds, "People");
            ViewBag.Table = new DataView(ds.Tables["People"]);

            return View();
        }

    }
}
