﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchemaFirstMVC.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            SampleDBCS context = new SampleDBCS();
            List<Customer> cList = new List<Customer>();
            foreach (Customer c in context.Customers)
            {
                cList.Add(c);
            }
            ViewBag.customerTable = cList;
            
            return View();
        }

    }
}
