﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCCRUDDemo
{
    public class Genre
    {
        [Required]
        public int GenreId { get; set; }
        [Required]
        public string GenreName { get; set; }

        public List<Movie> Movies { get; set; }
    }
}