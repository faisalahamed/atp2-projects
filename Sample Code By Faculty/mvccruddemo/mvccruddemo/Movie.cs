﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCCRUDDemo
{
    public class Movie
    {
        [Required]
        public int MovieId { get; set; }
        [Required]
        public string MovieName { get; set; }
        public int Year { get; set; }
        
       
        [ForeignKey("GenreId")]
        public List<Genre> Genres { get; set; }
    }
}