﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCCRUDDemo
{
    public class User
    {
        [Required]
        public int UserId { get; set; }
        [Required][MaxLength(30)][Index(IsUnique=true)]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string UserType { get; set; }

    }
}