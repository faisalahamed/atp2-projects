﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MVCCRUDDemo
{
    public class DbSeeder : DropCreateDatabaseIfModelChanges<DataContext>
    {
        public DbSeeder()
        {
           

        }
        protected override void Seed(DataContext context)
        {
            base.Seed(context);
            User firstUser = new User();
            firstUser.UserName = "admin";
            firstUser.Password = "admin";
            firstUser.UserType = "admin";

            context.Users.Add(firstUser);
            context.SaveChanges();
        }
    }
}