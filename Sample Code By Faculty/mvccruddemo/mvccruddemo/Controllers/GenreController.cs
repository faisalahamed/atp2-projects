﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCCRUDDemo.Controllers
{
    public class GenreController : BaseController
    {
        //
        // GET: /Genre/
        DataContext context = new DataContext();
        public ActionResult Index()
        {
            return View(context.Genres.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Genre g)
        {
            if (ModelState.IsValid)
            {
                context.Genres.Add(g);
                context.SaveChanges();
                return RedirectToAction("Index");

            }
            else
            {
                return View(g);
            }   
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Genre g = context.Genres.FirstOrDefault(i => i.GenreId == id);
            return View(g);
        }
        [HttpPost]
        public ActionResult Edit(Genre update_g)
        {
            if (ModelState.IsValid)
            {
                Genre g = context.Genres.FirstOrDefault(i => i.GenreId == update_g.GenreId);
                g.GenreName = update_g.GenreName;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View(update_g);
            }
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Genre g = context.Genres.FirstOrDefault(i => i.GenreId == id);
            return View(g);
        }
        [HttpPost][ActionName("Delete")]
        public ActionResult Delete_Confirm(int id)
        {
            Genre g = context.Genres.FirstOrDefault(i => i.GenreId == id);
            context.Genres.Remove(g);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            Genre g = context.Genres.Include("Movies").FirstOrDefault(i => i.GenreId == id);
            return View(g);
        }

    }
}
