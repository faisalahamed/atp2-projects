﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCCRUDDemo.Controllers
{
    public class MovieController : BaseController
    {
        //
        // GET: /Movie/
        DataContext context = new DataContext();

        public ActionResult Index()
        {
            return View(context.Movies.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {

            ViewBag.GenreList =context.Genres.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Create(Movie m)
        {
            string[] arr = Request["genre"].ToString().Split(new char[]{','});
            List<int> list = new List<int>();
            
            foreach(string id in arr)
            {
                list.Add(Convert.ToInt32(id));
            }

            int[] glist = list.ToArray<int>();

            List<Genre> genreList = context.Genres.Where(g => list.Contains(g.GenreId)).ToList();;

            if (ModelState.IsValid)
            {
                m.Genres = genreList;
                context.Movies.Add(m);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View(m);
            }
            
        }
    }
}
