﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCCRUDDemo.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["uname"] == null)
            {
                Response.Redirect("/LogIn");
            }
            base.OnActionExecuting(filterContext);
        }

    }
}
