﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCCRUDDemo.Controllers
{
    public class LogInController : Controller
    {
        //
        // GET: /LogIn/
        DataContext context = new DataContext();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(User user)
        {

            User validUser = context.Users.SingleOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);
            if (validUser == null)
            {
                TempData["error"] = "Invalid User or Password";
                return  RedirectToAction("Index");
            }
            else
            {
                Session["uname"] = validUser.UserName;
                Session["type"] = validUser.UserType;
                return RedirectToAction("Index", "Home");
            }            
        }
        public ActionResult Logout()
        {
            Session["uname"] = null;
            Session["type"] = null;
            return RedirectToAction("Index");
        }
    }
}



