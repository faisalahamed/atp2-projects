﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EF_CF_Demo.Controllers
{
    [Table("CourseTable")]
    public class Course
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourseCode { get; set; }
        [MaxLength(60), Required]
        public string CourseName { get; set; }
        public int Credit { get; set; }
        //public int FacultyId { get; set; }
        [ForeignKey("FacultyId")]
        public List<Faculty> faculties { get; set; }
        public int CourseCategory{get;set;}
    }
    public class Faculty
    {
        public int FacultyId { get; set; }
        public string FacultyName { get; set; }
        public int DepartmentId { get; set; }

        public Department department { get; set; }
        public List<Course> courses { get; set; }
    }
    public class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public List<Faculty> faculties { get; set; }

    }
    public class CourseDBContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Department> Departments { get; set; }
    }
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CourseDBContext>());
            CourseDBContext context = new CourseDBContext();

            /*
            Department d = new Department();
            d.DepartmentId = 1;
            d.DepartmentName = "CS";

            Faculty f = new Faculty();
            f.FacultyId = 1111;
            f.FacultyName = "ABCD";

            Course c = new Course();
            c.CourseId = 4201;
            c.CourseName = "ATP2";
            c.Credit = 3;

            context.Departments.Add(d);
            context.Faculties.Add(f);
            context.Courses.Add(c);

            context.SaveChanges();
            */
            
            /*
            Department update_d = context.Departments.FirstOrDefault(i=> i.DepartmentId==2);
            update_d.DepartmentName = "EEE";
            context.SaveChanges();
            */
            /*
            Course del_c = context.Courses.FirstOrDefault(i => i.CourseId == 3);
            context.Courses.Remove(del_c);
            context.SaveChanges();
            */
            ViewBag.contextObj = context;

            return View();
        }

    }
}




