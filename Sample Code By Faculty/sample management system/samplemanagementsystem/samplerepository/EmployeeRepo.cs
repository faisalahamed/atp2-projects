﻿using SampleEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleRepository
{
    public class EmployeeRepo : IEmployeeRepo
    {
        SampleDBContext context = new SampleDBContext();
        public List<Employee> GetAll()
        {
            return context.Employees.ToList();
        }
        public Employee Get(int id)
        {
            return context.Employees.SingleOrDefault(i => i.UserId == id);
        }
        public int Insert(Employee e)
        {
            context.Employees.Add(e);
            return context.SaveChanges();
        }

        public int Delete(Employee e)
        {
            context.Employees.Remove(e);
            return context.SaveChanges();
        }

        public int Update(Employee e)
        {
            context.Entry(e).State = System.Data.Entity.EntityState.Modified;
            return context.SaveChanges();
        }
    }
}
