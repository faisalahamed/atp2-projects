﻿using SampleEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleRepository
{
    public class CustomerRepo : ICustomerRepo
    {
        SampleDBContext context = new SampleDBContext();
        public List<Customer> GetAll()
        {
            return context.Customers.ToList();
        }
        public Customer Get(int id)
        {
            return context.Customers.SingleOrDefault(i => i.UserId == id);
        }
        public int Insert(Customer c)
        {
            context.Customers.Add(c);
            return context.SaveChanges();
        }
        public int Delete(Customer c)
        {
            context.Customers.Remove(c);
            return context.SaveChanges();
        }
        public int Update(Customer c)
        {
            context.Entry(c).State = System.Data.Entity.EntityState.Modified;
            return context.SaveChanges();
        }
    }
}
