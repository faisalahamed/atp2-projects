﻿using SampleEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleRepository
{
    public class DbSeeder : DropCreateDatabaseIfModelChanges<SampleDBContext>
    {
        protected override void Seed(SampleDBContext context)
        {
            base.Seed(context);

           /* User u1 = new User();
            u1.UserId = 1;
            u1.Password = "admin";
            u1.Type = "admin";

            User u2 = new User();
            u2.UserId = 2;
            u2.Password = "test";
            u2.Type = "customer";*/

            Employee e = new Employee();
          //  e.EmployeeId = u1.UserId;
            e.Password = "admin";
            e.Type = "admin";
            e.EmployeeName = "Admin1 Name";
            e.PhoneNumber = "+8801716661799";
            e.Salary = 5000.0;

            Customer c = new Customer();
          //  c.CustomerId = u2.UserId;
            c.Password = "test";
            c.Type = "customer";
            c.CustomerName = "Customer1 Name";
            c.PhoneNumber = "+8801316661799";
            c.Address = "Banani";

          //  context.Users.Add(u1);
            context.Employees.Add(e);
          //  context.Users.Add(u2);
            context.Customers.Add(c);


            context.SaveChanges();
            
        }
    }
}
