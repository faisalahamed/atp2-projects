﻿using SampleEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleRepository
{
    interface ICustomerRepo
    {
        List<Customer> GetAll();
        Customer Get(int id);
        int Insert(Customer c);
        int Delete(Customer c);
        int Update(Customer c);
    }
}
