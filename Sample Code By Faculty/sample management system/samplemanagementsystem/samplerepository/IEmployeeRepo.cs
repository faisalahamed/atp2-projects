﻿using SampleEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleRepository
{
    interface IEmployeeRepo
    {
        List<Employee> GetAll();
        Employee Get(int id);        
        int Insert(Employee e);
        int Delete(Employee e);
        int Update(Employee e);
    }
}
