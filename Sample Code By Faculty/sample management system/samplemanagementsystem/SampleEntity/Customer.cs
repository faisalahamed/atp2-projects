﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleEntity
{
    public class Customer : User
    {
       // [Required]
       // [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
       // public int CustomerId { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required][MaxLength(14)]
        public string PhoneNumber { get; set; }
        [Required]
        public string Address { get; set; }
    }
}
