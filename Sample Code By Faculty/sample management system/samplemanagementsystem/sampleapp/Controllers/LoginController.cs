﻿using SampleEntity;
using SampleRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleApp.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        SampleDBContext context = new SampleDBContext();
        [HttpGet]
        public ActionResult Index()
        {
            return View(); 
        }
        [HttpPost]
        public ActionResult Index(User user)
        {

            User validUser = context.Users.SingleOrDefault(u => u.UserId == user.UserId && u.Password == user.Password);
            if (validUser == null)
            {
                TempData["error"] = "Invalid User or Password";
                return RedirectToAction("Index");
            }
            else
            {
                Session["userid"] = validUser.UserId;
                Session["type"] = validUser.Type;
                if (validUser.Type.Equals("customer"))
                {
                    return RedirectToAction("Index", "Customer");
                }
                else
                {
                    return RedirectToAction("Index", "Employee");
                }
            }
        }
        public ActionResult Logout()
        {
            Session["userid"] = null;
            Session["type"] = null;
            return RedirectToAction("Index");
        }

    }
}
