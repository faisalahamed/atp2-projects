﻿using SampleEntity;
using SampleRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleApp.Controllers
{
    public class RegistrationController : Controller
    {
        //
        // GET: /Registration/
        CustomerRepo crepo = new CustomerRepo();

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Customer c)
        {
            if (ModelState.IsValid)
            {
                crepo.Insert(c);
                TempData["id"] = c.UserId;
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(c);
            }
        }

    }
}
