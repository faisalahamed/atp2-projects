﻿using SampleEntity;
using SampleRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleApp.Controllers
{
    public class EmployeeController : BaseController
    {
        //
        // GET: /Empployee/
        EmployeeRepo erepo = new EmployeeRepo();
        public ActionResult Index()
        {
            return View(erepo.GetAll());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee e)
        {
            if (ModelState.IsValid)
            {
                erepo.Insert(e);
                return RedirectToAction("Index");
            }
            else
            {
                return View(e);
            }
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Employee emp = erepo.Get(id);
            return View(emp);
        }
        [HttpPost]
        public ActionResult Edit(Employee emp)
        {
            if (ModelState.IsValid)
            {
                erepo.Update(emp);
                return RedirectToAction("Index");
            }
            else
            {
                return View(emp);
            }
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Employee emp = erepo.Get(id);
            return View(emp);
        }
        [HttpPost][ActionName("Delete")]
        public ActionResult Delete_Confirmed(int id)
        {
            Employee emp = erepo.Get(id);
            erepo.Delete(emp);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            Employee e = erepo.Get(id);
            return View(e);
        }
    }
}
