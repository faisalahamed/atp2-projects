namespace InventoryManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingOrderDetailsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        OrderId_OrderId = c.Int(),
                        ProductId_ProductId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId_OrderId)
                .ForeignKey("dbo.Products", t => t.ProductId_ProductId)
                .Index(t => t.OrderId_OrderId)
                .Index(t => t.ProductId_ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "ProductId_ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderDetails", "OrderId_OrderId", "dbo.Orders");
            DropIndex("dbo.OrderDetails", new[] { "ProductId_ProductId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId_OrderId" });
            DropTable("dbo.OrderDetails");
        }
    }
}
