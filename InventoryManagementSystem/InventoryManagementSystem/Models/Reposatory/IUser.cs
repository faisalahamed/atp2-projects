﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InventoryManagementSystem.Models;

namespace InventoryManagementSystem.Models
{
  
      public interface IUser
        {
            List<User> GetAll();
            User Get(int id);
            int Insert(User c);
            int Delete(User c);
            int Update(User c);
        }
    
}