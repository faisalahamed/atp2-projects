﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementSystem.Models
{
    public class UserRepo:IUser
    {
        InventoryDataContext context = new InventoryDataContext();
        public List<User> GetAll()
        {
            return context.Users.ToList();
        }
        public User Get(int id)
        {
            return context.Users.SingleOrDefault(i => i.UserId == id);
        }
        public int Insert(User c)
        {
            context.Users.Add(c);
            return context.SaveChanges();
        }
        public int Delete(User c)
        {
            context.Users.Remove(c);
            return context.SaveChanges();
        }
        public int Update(User c)
        {
            context.Entry(c).State = System.Data.Entity.EntityState.Modified;
            return context.SaveChanges();
        }
    }

    
}