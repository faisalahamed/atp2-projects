﻿using System.Linq;
using System.Web.Mvc;
using InventoryManagementSystem.Models;

namespace InventoryManagementSystem.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        InventoryDataContext context=new InventoryDataContext();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index(User user)
        {

            User validUser = context.Users.SingleOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);
            if (validUser == null)
            {
                TempData["error"] = "Invalid User or Password";
                return RedirectToAction("Index");
            }
            else
            {
                Session["username"] = validUser.UserName;
                Session["type"] = validUser.UserType;
                TempData["error"] = "Successful"+ Session["username"]+Session["type"];

                return RedirectToAction("Index");
            }
        }
    }
  

}