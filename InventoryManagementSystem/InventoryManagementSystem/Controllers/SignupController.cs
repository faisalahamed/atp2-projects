﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryManagementSystem.Models;



namespace InventoryManagementSystem.Controllers
{
    public class SignupController : Controller
    {
        // GET: Signup
        private UserRepo urepo = new UserRepo();
        [HttpGet]
        public ActionResult Signup()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Signup(User mUser)
        {
            if (ModelState.IsValid)
            {
                urepo.Insert(mUser);

                //TempData["id"] = context.Users;
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(mUser);
            }
        }
    }
}