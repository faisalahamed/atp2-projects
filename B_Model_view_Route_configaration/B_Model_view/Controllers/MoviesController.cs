﻿using B_Model_view.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B_Model_view.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Random()
        {
            var movie = new Movie() { Name = "Venom" ,MovieID=1};
            return View(movie);
        }
        public ActionResult edit(int ID)
        {
            return Content("ID:" + ID);
        }
        [Route("movies/ReleaseBydate/{year}/{month:regex(\\d{2}):range(01,12)}")]
        //only two digit month is possible and range is 01 to 12
        public ActionResult ReleaseBydate(int year,int month)
        {
            return Content(year + "/" + month);
        }
    }
}