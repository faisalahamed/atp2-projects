﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B_Model_view.Models
{
    public class Movie
    {
        public string Name { get; set; }
        public int MovieID { get; set; }
    }
}